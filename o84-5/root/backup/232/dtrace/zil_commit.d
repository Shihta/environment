#!/usr/sbin/dtrace -s
/*:zfs:zil_commit:entry
{
        stack(20);
}*/

:zfs:zil_commit:entry
{
        self->spec = speculation();
        speculate(self->spec);
}

:zfs:zil*:
/self->spec/
{
        speculate(self->spec);
}

:zfs:dmu*:
/self->spec && probefunc!="dmu_buf_get_user" && probefunc!="dmu_bonus_hold" && probefunc!="dmu_buf_hold" && probefunc!="dmu_read" && probefunc!="dmu_zfetch" && probefunc!="dmu_buf_hold_array_by_dnode" && probefunc!="dmu_objset_hold" && probefunc!="dmu_object_next" && probefunc!="dmu_buf_rele_array" && probefunc!="dmu_object_info" && probefunc!="dmu_zfetch_find" && probefunc!="dmu_objset_from_ds"/
{
        speculate(self->spec);
}

:zfs:spa_*:
/self->spec && probefunc!="spa_normal_class" && probefunc!="spa_strdup" && probefunc!="spa_dir_prop" && probefunc!="spa_close" && probefunc!="spa_load_verify_cb" && probefunc!="spa_strfree"/
{
        speculate(self->spec);
}

:zfs:zil_commit:return
/self->spec/
{
        commit(self->spec);
}




/*:zfs:zfs_ioc_pool_import:entry
{
	printf("zc_name=%10s -- zc_value=%10s -- zc_string=%10s -- zc_top_ds=%10s", args[0]->zc_name, args[0]->zc_value, args[0]->zc_string, args[0]->zc_top_ds);
}
:zfs:zil_check_log_chain:entry
{
        printf("osname=%s", stringof(args[0]));
}
::spa_check_logs:return
{
	printf("rt=%d\n", arg1);
}
::spa_check_logs:entry
{
	printf("name=%s", args[0]->spa_name);
}
::dmu_objset_find_spa:entry
/stringof(args[1]) == "zp2"/
{
	printf("args[1]=%s", stringof(args[1]));
	stack(20);
}
::dmu_objset_find_spa:entry
/stringof(args[1]) != "zp2"/
{
	printf("%s  !=zp2  >>  from:", stringof(args[1]));
}*/
