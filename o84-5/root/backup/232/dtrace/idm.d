#!/usr/sbin/dtrace -qs
::iscsit_abort:entry
{
	self->abort=1;
}

:iscsit::entry
/self->abort/
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}

:idm::entry
/self->abort/
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}

::iscsit_abort:return
{
	self->abort=0;
	printf("iscsit_abort ret=%lx\n", arg1);
	stack(20);
}

/*
::iscsit_abort:return
/self->abort && arg1/
{
	self->abort=0;
	stack(20);
}

::idm_task_abort_one:entry
{
	printf("idt_state=%d  abort_type=%d\n", args[1]->idt_state, args[2]);
}
*/

dtrace:::END
{
        printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
        printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}

