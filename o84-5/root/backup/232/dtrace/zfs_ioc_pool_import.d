#!/usr/sbin/dtrace -Fs
:zfs:zfs_ioc_pool_import:entry
{
	self->spec = speculation();
	speculate(self->spec);
}

:zfs:zil*:
/self->spec/
{
	speculate(self->spec);
}

:zfs:zfs*:
/self->spec/
{
	speculate(self->spec);
}

:zfs:d*:
/self->spec/
{
	speculate(self->spec);
}

:zfs:spa_*:
/self->spec/
{
	speculate(self->spec);
}

:zfs:zfs_ioc_pool_import:return
/self->spec/
{
	/*speculate(self->spec);*/
	commit(self->spec);
	/*self->spec=0;*/
}

