#!/usr/sbin/dtrace -Cs
#pragma D option quiet

dtrace:::BEGIN
{
        printf("Start...\n");
/*        printf("%10s  %10s  %26s  %20s %10s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");*/
}


pid$target::smb_kmod_stop:entry
{
	stack();
}
/*
pid$target::smb_kmod_ioctl:entry
{
	@share[probefunc,ustack()] = count();
}


pid$target::smb_kmod_share:return,
pid$target::smb_kmod_unshare:return
{
	printf("%s return value: %d\n\n", probefunc, arg1);
}

pid$target::smb_shr_remove:entry
{
	printf("remove share name: ");
	trace(copyinstr(arg0));
	printf("\n");
}
*/

/*
pid$target::smbd_door_return:entry
{
	trace(arg2);
	printf("%s\n", probefunc);
	trace(copyinstr(arg1));
	@share[probefunc,ustack()] = count();

}
*/
/*
fbt::smb*:entry
{
	@fbt[probeprov, probemod, probefunc, probename] = count();
}

dtrace:::END
{
        printf("%10s  %15s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
        printa("%10s  %15s  %30s  %30s  %@6d\n", @fbt);
        printf("\n\n");
        printa("%10s  %15s  %30s  %30s  %@6d\n", @pidd);
        printf("\n\n");
	printa(@share);
}*/
