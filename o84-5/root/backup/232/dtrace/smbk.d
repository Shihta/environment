#!/usr/sbin/dtrace -Fs
::smb_drv_ioctl:entry
{
	/*self->ioctl=1;*/
	self->ioctl=speculation();
	speculate(self->ioctl);
}

::smb_drv_ioctl:return
/self->ioctl/
{
	commit(self->ioctl);
	self->ioctl=0;
}

fbt:smbsrv::
/self->ioctl && probefunc != "smb_drv_ioctl" && probefunc != "smb_avl_hold"/
{
	/*printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);*/
	speculate(self->ioctl);
	trace(errno);
	/*@pidd[probeprov, probemod, probefunc, probename] = count();*/
}
/*
dtrace:::END
{
        printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
        printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}
*/
