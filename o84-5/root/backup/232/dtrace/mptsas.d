#!/usr/sbin/dtrace -s

::mptsas_scsi_start:entry
{
        stack(20);
}

/*
:mpt_sas:*attach*:entry
{
	@pidd[probeprov, probemod, probefunc, probename] = count();
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
}

:sd:*attach*:entry
{
	@pidd[probeprov, probemod, probefunc, probename] = count();
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
}

:scsi:*attach*:entry
{
	@pidd[probeprov, probemod, probefunc, probename] = count();
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
}

:mpt_sas:*probe*:entry
{
	@pidd[probeprov, probemod, probefunc, probename] = count();
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
}

:sd:*probe*:entry
{
	@pidd[probeprov, probemod, probefunc, probename] = count();
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
}

:scsi_vhci::entry
/ probefunc != "vhci_scsi_bus_unconfig" /
{
	@pidd[probeprov, probemod, probefunc, probename] = count();
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
	stack(20)
}

:scsi:*probe*:entry
{
	@pidd[probeprov, probemod, probefunc, probename] = count();
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
}

dtrace:::END
{
        printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
        printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}
*/
