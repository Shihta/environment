#!/usr/sbin/dtrace -Fs
:zfs:spa_load_impl:entry
{
	self->spec = speculation();
	speculate(self->spec);
}

:zfs:zil*:
/self->spec/
{
	speculate(self->spec);
}

:zfs:dmu*:
/self->spec && probefunc!="dmu_buf_get_user" && probefunc!="dmu_bonus_hold" && probefunc!="dmu_buf_hold" && probefunc!="dmu_read" && probefunc!="dmu_zfetch" && probefunc!="dmu_buf_hold_array_by_dnode" && probefunc!="dmu_objset_hold" && probefunc!="dmu_object_next" && probefunc!="dmu_buf_rele_array" && probefunc!="dmu_object_info" && probefunc!="dmu_zfetch_find" && probefunc!="dmu_objset_from_ds"/
{
	speculate(self->spec);
}

:zfs:spa_*:
/self->spec && probefunc!="spa_normal_class" && probefunc!="spa_strdup" && probefunc!="spa_dir_prop" && probefunc!="spa_close" && probefunc!="spa_load_verify_cb" && probefunc!="spa_strfree"/
{
	speculate(self->spec);
}

:zfs:spa_load_impl:return
/self->spec/
{
	/*speculate(self->spec);*/
	commit(self->spec);
	/*self->spec=0;*/
}

