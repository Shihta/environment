#!/usr/sbin/dtrace -s

#pragma D option quiet

dtrace:::BEGIN
{
	printf("Start...\n");
	printf("%10s  %10s  %26s  %20s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE");
}

pid$target:::entry
/probemod != "libc.so.1" && probemod != "libumem.so.1" && probemod != "LM1`ld.so.1"/
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}
dtrace:::END
{
	printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
	printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}

