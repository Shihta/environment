#!/usr/sbin/dtrace -qs
:stmf_sbd::entry
{
/*	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);*/
	@pidd[probeprov, probemod, probefunc, probename] = count();
/*        stack(20);*/
}

::sbd_is_zvol:return
{
	printf("is_zfs=%d", arg1);
}

dtrace:::END
{
        printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
        printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}

