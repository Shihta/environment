#!/usr/sbin/dtrace -s

#pragma D option quiet

dtrace:::BEGIN
{
	printf("Start...\n");
	printf("%10s  %10s  %26s  %20s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE");
}

pid$target:zdb::entry
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}
/*
fbt:zfs::entry
/execname == "zdb"/
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	@fbt[probeprov, probefunc, probename] = count();
}

sdt:zfs::
/execname == "zdb"/
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	@sdt[probeprov, probefunc, probename] = count();
}
*/
dtrace:::END
{
	printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
/*	printa("%10s  %30s  %30s  %@6d\n", @fbt);
	printf("\n\n");
	printa("%10s  %30s  %30s  %@6d\n", @sdt);
	printf("\n\n");*/
	printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}

/*pid$target:zfs:print_log_block:entry
{
	stack(20);
}*/
