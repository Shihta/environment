#!/usr/sbin/dtrace -Fs
:zfs:zil_check_log_chain:entry
{
	self->spec = speculation();
	speculate(self->spec);
}

:zfs:zil*:
/self->spec/
{
	speculate(self->spec);
}

:zfs:dmu*:
/self->spec/
{
	speculate(self->spec);
}

:zfs:spa_l*:
/self->spec/
{
	speculate(self->spec);
}

:zfs:spa_c*:
/self->spec/
{
	speculate(self->spec);
}

:zfs:zil_check_log_chain:return
/self->spec/
{
	/*speculate(self->spec);*/
	commit(self->spec);
	/*self->spec=0;*/
}

