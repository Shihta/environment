#!/usr/sbin/dtrace -qs

::smb_server_destroy_session:
{
	stack();
}

::smb_soshutdown:, ::smb_sodestroy:, ::smb_server_listener:, ::smb_server_receiver:, ::smb_server_destroy_session:
{
	printf("%10d  %38s  %8s\n", curthread->t_did, probefunc, probename);
}

::smb_soshutdown:, ::smb_sodestroy:
{
	printf(">>>>\n%10d  %38s  %8s\n", curthread->t_did, probefunc, probename);
	stack();
	printf("soaddr=%lx\n<<<<\n", arg0);
}

/*
dtrace:::END
{
        printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
        printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}
*/
