#!/usr/sbin/dtrace -s
:zfs:zil_parse:entry
{
        stack(20);
        self->zil=args[0];
}

:zfs:zil_parse:return
/self->zil/
{
	printf("lr_count=%d  blk_count=%d  lr_seq=%d  blk_seq=%d", (self->zil)->zl_parse_lr_count, (self->zil)->zl_parse_blk_count, (self->zil)->zl_parse_lr_seq, (self->zil)->zl_parse_blk_seq );
	self->zil=0;
}

:zfs:zil_replay:entry
{
	stack(20);
	printf("zh_flags=%x", ((args[0]->os_zil)->zl_header)->zh_flags  )
}

:zfs:zil_incr_blks:entry
{
	stack(20);
}

:zfs:zil_replay_log_record:entry
{
	stack(20);
}

:zfs:zvol_replay_write:entry
{
	stack(20);
}

/*
:zfs:zfs_ioc_pool_import:entry
{
	printf("zc_name=%10s -- zc_value=%10s -- zc_string=%10s -- zc_top_ds=%10s", args[0]->zc_name, args[0]->zc_value, args[0]->zc_string, args[0]->zc_top_ds);
}
:zfs:zil_check_log_chain:entry
{
        printf("osname=%s", stringof(args[0]));
}
::spa_check_logs:return
{
	printf("rt=%d\n", arg1);
}
::spa_check_logs:entry
{
	printf("name=%s", args[0]->spa_name);
}
::dmu_objset_find_spa:entry
/stringof(args[1]) == "zp2"/
{
	printf("args[1]=%s", stringof(args[1]));
	stack(20);
}
::dmu_objset_find_spa:entry
/stringof(args[1]) != "zp2"/
{
	printf("%s  !=zp2  >>  from:", stringof(args[1]));
}
*/
