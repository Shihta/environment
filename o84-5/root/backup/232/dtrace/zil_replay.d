#!/usr/sbin/dtrace -s
/*
:zfs:zil_parse:entry
{
        stack(20);
        self->zil=args[0];
}

:zfs:zil_parse:return
/self->zil/
{
	printf("lr_count=%d  blk_count=%d  lr_seq=%d  blk_seq=%d", (self->zil)->zl_parse_lr_count, (self->zil)->zl_parse_blk_count, (self->zil)->zl_parse_lr_seq, (self->zil)->zl_parse_blk_seq );
	self->zil=0;
}
*/
:zfs:zil_replay:entry
{
	self->zil_replay=1;
}
:zfs:zil_replay:return
/ self->zil_replay==1 /
{
	stack(20);
        self->zil_replay=0;
}

:zfs:zil_destroy:entry
/ self->zil_replay==1 /
{
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}

:zfs:zil_replay_log_record:entry
/ self->zil_replay==1 /
{
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}
:zfs:zil_incr_blks:entry
/ self->zil_replay==1 /
{
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}
dtrace:::END
{
	printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
	printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}

/*
:zfs:zvol_replay_write:entry
{
	stack(20);
}
*/
