#!/usr/sbin/dtrace -s
::pageio_setup:entry
{
        stack(20);
}

::bio_pageio_done:entry
{
        stack(20);
}
/*
::biodone:entry
{
        stack(20);
}
*/
/*
:zfs:zil_replay:entry
{
        self->spec = speculation();
        speculate(self->spec);
}

:zfs:zil*:
/self->spec/
{
        speculate(self->spec);
}

:zfs:dmu*:
{
        speculate(self->spec);
}

:zfs:spa_*:
{
        speculate(self->spec);
}

:zfs:zil_replay:return
/self->spec/
{
        commit(self->spec);
}
*/

/*
:zfs:zfs_ioc_pool_import:entry
{
	printf("zc_name=%10s -- zc_value=%10s -- zc_string=%10s -- zc_top_ds=%10s", args[0]->zc_name, args[0]->zc_value, args[0]->zc_string, args[0]->zc_top_ds);
}
:zfs:zil_check_log_chain:entry
{
        printf("osname=%s", stringof(args[0]));
}
::spa_check_logs:return
{
	printf("rt=%d\n", arg1);
}
::spa_check_logs:entry
{
	printf("name=%s", args[0]->spa_name);
}
::dmu_objset_find_spa:entry
/stringof(args[1]) == "zp2"/
{
	printf("args[1]=%s", stringof(args[1]));
	stack(20);
}
::dmu_objset_find_spa:entry
/stringof(args[1]) != "zp2"/
{
	printf("%s  !=zp2  >>  from:", stringof(args[1]));
}
*/
