#!/usr/sbin/dtrace -s
:sd:*read:entry
{
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}

:sd:*write:entry
{
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}

:sd:sdstrategy:entry
{
	printf("%10s  %10s  %26s  %20s", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}

dtrace:::END
{
        printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
        printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}

