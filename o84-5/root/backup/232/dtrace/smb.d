#!/usr/sbin/dtrace -qs

::smbsr_map_errno:entry
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}

::smb_opipe_read:entry
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}

::smb_opipe_write:entry
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}

::smb_opipe_read:return
{
	printf("read return=%d\n", arg1);
}

::smb_opipe_write:return
{
	printf("write return=%d\n", arg1);
}

::smb_opipe_transact:entry
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	@pidd[probeprov, probemod, probefunc, probename] = count();
}

dtrace:::END
{
        printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
        printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}

