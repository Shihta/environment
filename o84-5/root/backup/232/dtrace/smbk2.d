#!/usr/sbin/dtrace -qs
::smb_kshare_unexport:entry
{
	self->un=1;
	printf("shrname=%s\n", stringof(args[0]));
}

::smb_kshare_unexport:return
/self->un/
{
	self->un=0;
	printf("rc=%d\n", arg1);
}

::smb_avl_release:entry
/self->un/
{
	/*printf("smb_avl_release shr_autocnt=%d  shr_flags=%x\n", args[1]->shr_autocnt, args[1]->shr_flags);*/
	shr=(smb_kshare_t *)args[1];
	printf("smb_avl_release shr_autocnt=%d  shr_flags=%x\n", shr->shr_autocnt, shr->shr_flags);
}

::smb_server_sharevp:entry, ::smb_vfs_rele:entry, ::smb_avl_remove:entry, ::smb_vfs_destroy:entry
/self->un/
{
	printf("%10s  %10s  %26s  %20s\n", probeprov, probemod, probefunc, probename);
	/*@pidd[probeprov, probemod, probefunc, probename] = count();*/
}

::smb_vfs_rele:smb_vfs_release
/self->un/
{
	vfs=(smb_vfs_t *)arg0;
	printf("sv_refcnt=%d\n", vfs->sv_refcnt);
}

::vn_rele:entry
/self->un/
{
	printf("v_count=%d\n", args[0]->v_count);
}

/*
dtrace:::END
{
        printf("%10s  %10s  %30s  %30s  %6s\n", "PROVIDER", "MODULE", "FUNCTION", "PROBE", "COUNT");
        printa("%10s  %10s  %30s  %30s  %@6d\n", @pidd);
}
*/
