set background=dark
syntax on
set bs=2
set hls
map <C-n> :tabnext<CR>
map <C-p>  :tabprevious<CR>
set tabpagemax=300
