tcm_node --block iblock_0/my_iblock0 /dev/sda
tcm_node --addtgptgp=iblock_0/my_iblock0 lio_alua_west
tcm_node --addtgptgp=iblock_0/my_iblock0 lio_alua_east
lio_node --addlun iscsi-test-west 1 1 my_west_lun iblock_0/my_iblock0
lio_node --addlun iscsi-test-east 1 1 my_east_lun iblock_0/my_iblock0
lio_node --settgptgp=iscsi-test-west 1 1 lio_alua_west
lio_node --settgptgp=iscsi-test-east 1 1 lio_alua_east
lio_node --addnp iscsi-test-west 1 192.168.30.101:3260
lio_node --addnp iscsi-test-east 1 192.168.30.102:3260
lio_node --demomode iscsi-test-west 1
lio_node --demomode iscsi-test-east 1
lio_node --disableauth iscsi-test-west 1
lio_node --disableauth iscsi-test-east 1
echo 0 > /sys/kernel/config/target/iscsi/iscsi-test-west/tpgt_1/attrib/demo_mode_write_protect
echo 0 > /sys/kernel/config/target/iscsi/iscsi-test-east/tpgt_1/attrib/demo_mode_write_protect
#lio_node --addlunacl iscsi-test-west 1 iqn.2011-03.tw.aiden:initiator 1 1
#lio_node --addlunacl iscsi-test-east 1 iqn.2011-03.tw.aiden:initiator 1 1
lio_node --enabletpg iscsi-test-west 1
lio_node --enabletpg iscsi-test-east 1
tcm_node --setaluapref iblock_0/my_iblock0 lio_alua_west 
tcm_node --listaluatpgs iblock_0/my_iblock0
